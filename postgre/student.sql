CREATE TABLE "student" (
  "id" bigserial PRIMARY KEY,
  "full_name" varchar NOT NULL,
  "gender" varchar NOT NULL,
  "email" varchar NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);

CREATE INDEX ON "student" ("id");
