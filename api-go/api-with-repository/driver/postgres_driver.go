package driver

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type PostgresDB struct {
	SQL *sql.DB
}

var Postgres = &PostgresDB{}

func Connect(host, port, user, password , dbname string) (*PostgresDB) {
	connectStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", 
								host, port, user, password , dbname)

	db, err := sql.Open("postgres", connectStr)
	if err != nil {
		panic(err) // dừng thực thi hàm
	}

	Postgres.SQL = db

	return Postgres
}