package main

import (
	"api-repository/driver"
	models "api-repository/model"
	"api-repository/repository/repoimpl"
	"fmt"
)

// host, port, user, password , dbname

const (
	host = "localhost"
	port = "5432"
	user = "admin"
	password = "admin"
	dbname = "postgres"
)

func main() {
	db := driver.Connect(host, port, user, password , dbname)

	err := db.SQL.Ping()

	if err != nil {
		panic(err)
	}

	fmt.Println("connect success")

	studentRepo := repoimpl.NewStudentRepo(db.SQL)

	uhp := models.Student{
		ID: 1,
		FullName: "Nguyen Van A",
		Gender: "Male",
		Email: "uhp@gmail.com",
	}

	dt := models.Student{
		ID: 2,
		FullName: "Nguyen Van B",
		Gender: "Male",
		Email: "dt@gmail.com",
	}

	studentRepo.Insert(&uhp)
	studentRepo.Insert(&dt)

	users, _ := studentRepo.Select()

	for i:= range users {
		fmt.Println(users[i])
	}
}