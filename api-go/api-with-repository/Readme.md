### Repository pattern

Chia làm 3 phần

Business Logic --> Repository (interface) --> Table repo --> Database

### Example:

Muốn tương tác với Student Data nằm trong db
phải đi qua
repository (student_repo.go)  
table repo (student_repo.impl.go)
model (student.go)

