package student

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Check(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, "success")
}