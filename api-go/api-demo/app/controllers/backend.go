package controllers

import (
	"errors"
	"math"
	"strings"

	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
)

type Request struct {
	Student string    `json:"student"`
	Class 	string 	  `json:"class"`
}

func ResponseSuccess(c *gin.Context, code int, message string, data interface{}) {

	c.AbortWithStatusJSON(code, gin.H{
		"code":         code,
		"data":         data,
		"message":      "",
		"message_code": message,
		"version":      "",
		"request_id":   requestid.Get(c),
	})
	panic(nil)
}

func ResponseError(c *gin.Context, code int, message string, err error) {

	if true {
		err = nil
	}
	if err == nil {
		err = errors.New("")
	}
	c.AbortWithStatusJSON(code, gin.H{
		"code":         code,
		"error":        err.Error(),
		"message":      "",
		"message_code": message,
		"version":      "",
		"request_id":   requestid.Get(c),
	})
	panic(nil)
}

type Pagination struct {
	Page       int64    `json:"current" form:"current"`
	Length     int64    `json:"pageSize" form:"pageSize"`
	Search     string   `json:"search" form:"search"`
	Sorts      []string `json:"sorts" form:"sorts[]"`
	Filters    []string `json:"filters" form:"filters[]"`
	Total      int64    `json:"total" form:"total"`
	TotalPages float64  `json:"total_pages" form:"-"`
}

func (u *Pagination) Sort() string {
	sort := ""
	for _, t := range u.Sorts {
		sort += ", " + strings.ReplaceAll(t, "__", " ")
	}
	if len(sort) > 0 {
		return sort[1:]
	}
	return "id desc"
}

func (u *Pagination) HandleTotalPage() {
	u.TotalPages = math.Ceil(float64(u.Total) / float64(u.Length))
}
func (u *Pagination) Filter() string {
	sort := ""
	for _, t := range u.Filters {
		sort += ", " + strings.ReplaceAll(t, "__", " ")
	}
	if len(sort) > 0 {
		return sort[1:]
	}
	return ""
}
