package routerProvider

import (
	"fmt"

	"demo/app/middlewares"
	"demo/routes"

	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
)

func Init(router *gin.Engine) {
		fmt.Println("------------------------------------------------------------")

		router.Use(gin.Recovery())
		router.Use(middlewares.CorsMiddleware())
		router.Use(requestid.New())
		router.Use(middlewares.ConfigsMiddleware())

		api := router.Group("api/v1")

		routes.StudentRouters(api.Group("student"))

}