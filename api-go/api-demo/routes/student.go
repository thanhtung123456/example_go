package routes

import (
	"demo/app/controllers/student"
	"demo/app/middlewares"

	"github.com/gin-gonic/gin"
)

func StudentRouters(router *gin.RouterGroup) {	
	router.Use(middlewares.AuthorizationMiddleware())
	router.GET("students",student.Check)
	router.GET("student/:id",student.Check)
	router.POST("student",student.Create)
	router.PUT("student/:id",student.Update)
}